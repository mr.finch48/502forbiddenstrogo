<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::post('/live_stream', 'Api\LiveStreamController@store');
    Route::get('/live_stream', 'Api\LiveStreamController@indexAdmin');
    Route::put('/live_stream/{id}', 'Api\LiveStreamController@update');
    Route::delete('/live_stream/{id}', 'Api\LiveStreamController@destroy');
    Route::get('/live_stream', 'Api\LiveStreamController@index');
    Route::get('/live_stream/{id}', 'Api\LiveStreamController@show');

    Route::post('/nominations', 'Api\NominationController@store');
    Route::put('/nominations/{id}', 'Api\NominationController@update');
    Route::delete('/nominations/{id}', 'Api\NominationController@destroy');
    Route::get('/nominations', 'Api\NominationController@index');
    Route::get('/nominations/{id}', 'Api\NominationController@show');
});