<?php


namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Illuminate\Support\Facades\Log;

trait ModelEventLogger
{

    public function log($message) {
        Log::channel('files')->info($message);
    }

}
