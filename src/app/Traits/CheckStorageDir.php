<?php

namespace App\Traits;


use Illuminate\Support\Facades\Storage;

trait CheckStorageDir
{

    public function createIsDirNotExist(string $path) {
        if (!Storage::disk('public')->exists($path)) {
            Storage::disk('public')->makeDirectory($path);
        }
    }

}