<?php

namespace App\Http\Controllers\Api;

use App\Builders\LiveStreamBuilder;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLiveStreamRequest;
use App\Http\Resources\LiveStreamCollection;
use App\Models\LiveStream;
use App\Repositories\LiveStreamRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LiveStreamController extends Controller
{

    private $repository;

    private $builder;

    public function __construct()
    {
        $this->repository = new LiveStreamRepository();
        $this->builder = new LiveStreamBuilder();
    }


    public function index(Request $request)
    {
        return (new LiveStreamCollection(
            $this->repository->getAllForPublic(true, $request, $request->header('localization') ?? 'ru')
        ))->additional([
            'total' => $this->repository->getCountForPagination($request)
        ]);
    }

    public function indexAdmin(Request $request)
    {
        return (new LiveStreamCollection(
            $this->repository->getAllForAdmin(true, $request)
        ))->additional([
            'total' => $this->repository->getCountForAdminPagination($request)
        ]);
    }


    public function show(Request $request)
    {
        return $this->repository->getByIdForPublic(
            $request->route('id'),
            $request->header('localization') ?? 'ru'
        );
    }


    public function store(CreateLiveStreamRequest $request)
    {
        $this->builder
            ->createEmptyModel()
            ->setLiveStream($request->live_stream)
            ->setTitle($request->title)
            ->setTitleEng($request->title_eng)
            ->setText($request->text)
            ->setTextEng($request->text_eng)
            ->setDescription($request->description)
            ->setDescriptionEng($request->description_eng)
            ->setStartDateTime($request->start_date_time)
            ->setEndDateTime($request->end_date_time)
            ->setRubric($request->rubric)
            ->setTagsList($request->tags_list)
            ->setPreviewMedia($request->preview_media)
            ->saveModel();

        return $this->builder->getModel();
    }


    public function update(CreateLiveStreamRequest $request, $id)
    {
        if ($live_stream = $this->repository->getByIdForPublic($id)) {
            $this->builder
                ->loadModel($live_stream)
                ->setLiveStream($request->live_stream)
                ->setTitle($request->title)
                ->setTitleEng($request->title_eng)
                ->setText($request->text)
                ->setTextEng($request->text_eng)
                ->setDescription($request->description)
                ->setDescriptionEng($request->description_eng)
                ->setStartDateTime($request->start_date_time)
                ->setEndDateTime($request->end_date_time)
                ->setRubric($request->rubric)
                ->setTagsList($request->tags_list)
                ->setPreviewMedia($request->preview_media)
                ->saveModel();

            return $this->builder->getModel();
        }
    }


    public function destroy(int $id)
    {
        LiveStream::destroy($id);

        return response()->json('', Response::HTTP_OK);
    }
}
