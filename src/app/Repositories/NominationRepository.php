<?php

namespace App\Repositories;


use App\Models\Announcement;
use App\Models\CoreModel;
use App\Models\LiveStream;
use App\Models\Media;
use App\Models\Nomination;
use App\Traits\IncrementView;

class NominationRepository extends CoreRepository
{

    protected function getModel(): CoreModel
    {
        return new Nomination();
    }


    protected function getFilterRules(): array
    {
        return [];
    }


    protected function getOrder(): array
    {
        return [
            'column' => 'id',
            'type' => 'asc'
        ];
    }

}
