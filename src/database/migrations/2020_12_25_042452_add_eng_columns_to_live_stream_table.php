<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEngColumnsToLiveStreamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('live_stream', function (Blueprint $table) {
            $table->string('title_eng', 255)->nullable(true);
            $table->string('description_eng', 255)->nullable(true);
            $table->text('text_eng')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('live_stream', function (Blueprint $table) {
            //
        });
    }
}
