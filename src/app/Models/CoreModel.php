<?php

namespace App\Models;


use App\Traits\Observable;
use Illuminate\Database\Eloquent\Model;

abstract class CoreModel extends Model
{
    use Observable;

    abstract static public function getPublicFields();

    abstract static public function getAdminFields();

    public function scopeForPublic($query)
    {
        return $query->select($this->getPublicFields());
    }

    public function scopeForPublicEng($query)
    {
        return $query->select($this->getPublicEngFields());
    }

    public function scopeForAdmin($query)
    {
        return $query->select($this->getAdminFields());
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%' . $value . '%');
    }

    public function scopeWhereIlike($query, $column, $value)
    {
        return $query->where($column, 'ilike', '%' . $value . '%');
    }

    public function scopeOrWhereIlike($query, $column, $value)
    {
        return $query->orWhere($column, 'ilike', '%' . $value . '%');
    }

}
