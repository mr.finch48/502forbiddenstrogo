<?php

namespace App\Http\Resources;


trait PaginatedResource
{
    public function with($request)
    {
        return [
            'count' => $this->collection->count(),
            'offset' => (int)$request->get('offset'),
            'limit' => (int)$request->get('limit'),
        ];
    }

}