<?php

namespace App\Traits;


use App\Models\CoreModel;
use Illuminate\Database\Eloquent\Builder;

trait Paginatier
{

    protected function paginate(Builder $query_builder): Builder
    {
        if ($this->isPaginationParamsExist()) {
            $query_builder = $this->setPaginationParams($query_builder);
        }

        return $query_builder;
    }


    private function setPaginationParams(Builder $query_builder): Builder
    {
        return $query_builder
            ->skip($this->getOffsetParam())
            ->take($this->getLimitParam());
    }


    protected function isPaginationParamsExist(): bool
    {
        return (!is_null($this->getOffsetParam()) && !is_null($this->getLimitParam()));
    }


    private function getOffsetParam(): ?int
    {
        return $_GET['offset'] ?? null;
    }


    private function getLimitParam(): ?int
    {
        return $_GET['limit'] ?? null;
    }

}