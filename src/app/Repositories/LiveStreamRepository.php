<?php

namespace App\Repositories;


use App\Models\Announcement;
use App\Models\CoreModel;
use App\Models\LiveStream;
use App\Models\Media;
use App\Traits\AdminLiveStreamFilter;
use App\Traits\IncrementView;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class LiveStreamRepository extends CoreRepository
{

    use IncrementView, AdminLiveStreamFilter;

    protected function getModel(): CoreModel
    {
        return new LiveStream();
    }

    protected function getOrder(): array
    {
        return [
            'column' => 'start_date_time',
            'type' => 'desc'
        ];
    }

    protected function getFilterRules(): array
    {
        return [
            [
                'parameter' => 'year',
                'comparison' => 'like',
                'fields' => ['start_date_time','end_date_time']
            ],
            [
                'parameter' => 'key',
                'comparison' => 'like',
                'fields' => ['title', 'description', 'tags_list']
            ],
            [
                'parameter' => 'rubric',
                'comparison' => '=',
                'fields' => ['rubric']
            ],
            [
                'parameter' => 'tagList',
                'comparison' => 'like_json',
                'fields' => ['tags_list']
            ],
        ];
    }

    public function getAllForAdmin(bool $paginated = false, Request $request_for_filter = null): Collection
    {
        $query_builder = $this->getModel()->forAdmin();

        if (method_exists($this, 'getOrder')) {
            $order = $this->getOrder();
            $query_builder = $query_builder->orderBy($order['column'], $order['type']);
        } else {
            $query_builder = $query_builder->orderBy('id', 'DESC');
        }

        $query_builder = $this->filterAdminLiveStream($query_builder, $request_for_filter);

        if ($paginated) {
            $query_builder = $this->paginate($query_builder);
        }

        return $query_builder->get();
    }

    public function getCountForAdminPagination($request_for_filter = null): int
    {
        $query_builder = $this->getModel()->select(['id']);

        $query_builder = $this->filterAdminLiveStream($query_builder, $request_for_filter);

        return $query_builder->get()->count();
    }

}
