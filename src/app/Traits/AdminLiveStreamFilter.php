<?php

namespace App\Traits;


trait AdminLiveStreamFilter
{
    public function filterAdminLiveStream($query_builder, $request_for_filter)
    {

        if ($key = $request_for_filter->get('key')) {
            $query_builder = $query_builder->where(function ($query) use ($key) {
                $query->whereLike('title', $key);
                $query->orWhereLike('description', $key);
                $query->orWhereJsonContains('tags_list', $key);
            });
        }

        if ($date_start = $request_for_filter->get('date_start')) {
            $query_builder = $query_builder->where(function ($query) use ($date_start) {
                $query->where('start_date_time', '>=', $date_start);
                $query->orWhere('end_date_time', '>=', $date_start);
            });
        }

        if ($date_end = $request_for_filter->get('date_end')) {
            $query_builder = $query_builder->where(function ($query) use ($date_start) {
                $query->where('start_date_time', '>=', $date_start);
                $query->orWhere('end_date_time', '>=', $date_start);
            });
        }

        if ($rubric = $request_for_filter->get('rubric')) {
            $query_builder = $query_builder->whereRubric($rubric);
        }

        return $query_builder;
    }
}