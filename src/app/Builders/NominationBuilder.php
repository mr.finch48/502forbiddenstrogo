<?php

namespace App\Builders;


use App\Models\Nomination;
use App\Traits\CheckStorageDir;
use App\Traits\ModelEventLogger;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class NominationBuilder extends CoreBuilder
{

    use CheckStorageDir, ModelEventLogger;

    private const IMAGES_STORAGE = '/images/nomination_images/';

    public function createEmptyModel(): CoreBuilder
    {
        $this->model = new Nomination();

        return $this;
    }

    public function setImage(UploadedFile $file = null)
    {
        if ($file) {
            $this->createIsDirNotExist(self::IMAGES_STORAGE);

            $file_name = Str::random(32) . "." . $file->extension();

            $this->model->image = $file_name;

            Storage::disk('public')->putFileAs(self::IMAGES_STORAGE, $file, $file_name);

            $this->log("Upload file: " . self::IMAGES_STORAGE . $file_name);
        }

        return $this;
    }
}
