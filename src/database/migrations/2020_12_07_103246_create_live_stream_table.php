<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLiveStreamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_stream', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('start_date_time');
            $table->dateTime('end_date_time');
            $table->string('preview_media', 255)->nullable();
            $table->string('live_stream', 255)->nullable();
            $table->string('title', 255);
            $table->string('description', 255);
            $table->text('text')->nullable();
            $table->json('tags_list')->nullable();

            $table->integer('rubric');
            $table->foreign('rubric')->references('id')->on('rubric')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->bigInteger('creator');
            $table->foreign('creator')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->bigInteger('view_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_stream');
    }
}
