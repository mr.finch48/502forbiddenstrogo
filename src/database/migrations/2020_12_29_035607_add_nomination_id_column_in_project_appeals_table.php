<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNominationIdColumnInProjectAppealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_appeals', function (Blueprint $table) {
            $table->integer('nomination_id')->nullable();
            $table->foreign('nomination_id')->references('id')->on('nominations')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_appeal', function (Blueprint $table) {
            //
        });
    }
}
