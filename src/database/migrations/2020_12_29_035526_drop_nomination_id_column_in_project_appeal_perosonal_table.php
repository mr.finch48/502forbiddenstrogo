<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropNominationIdColumnInProjectAppealPerosonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_appeal_personal', function (Blueprint $table) {
            $table->dropForeign(['nomination_id']);
            $table->dropColumn('nomination_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_appeal_personal', function (Blueprint $table) {
            //
        });
    }
}
