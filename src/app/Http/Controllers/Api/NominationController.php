<?php

namespace App\Http\Controllers\Api;

use App\Builders\NominationBuilder;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNominationRequest;
use App\Repositories\NominationRepository;
use Illuminate\Http\Response;

class NominationController extends Controller
{

    private $repository;

    private $builder;


    public function __construct()
    {
        $this->repository = new NominationRepository();
        $this->builder = new NominationBuilder();
    }


    public function index()
    {
        return $this->repository->getAllForPublic();
    }


    public function show(int $id)
    {
        return $this->repository->getByIdForPublic($id);
    }


    public function store(CreateNominationRequest $request)
    {
        $this->builder
            ->createEmptyModel()
            ->setTitle($request->title)
            ->setDescription($request->description)
            ->setImage($request->image)
            ->saveModel();

        return $this->builder->getModel();
    }


    public function update(CreateNominationRequest $request, $id)
    {
        if ($nomination = $this->repository->getByIdForAdmin($id)) {
            $this->builder
                ->loadModel($nomination)
                ->setTitle($request->title)
                ->setDescription($request->description)
                ->setImage($request->image)
                ->saveModel();
        }

        return $this->builder->getModel();
    }


    public function destroy(int $id)
    {
        $nomination = $this->repository->getByIdForAdmin($id);

        $nomination->delete();

        return response()->json('', Response::HTTP_OK);
    }
}
