<?php

namespace App\Builders;


use App\Models\LiveStream;
use App\Models\News;
use App\Traits\CheckStorageDir;
use App\Traits\ModelEventLogger;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class LiveStreamBuilder extends CoreBuilder
{

    use CheckStorageDir, ModelEventLogger;

    private const PREVIEWS_STORAGE = '/images/live_stream_previews/';

    public function createEmptyModel(): CoreBuilder
    {
        $this->model = new LiveStream();

        return $this;
    }

    public function setPreviewMedia(UploadedFile $file = null)
    {
        if ($file) {
            $this->createIsDirNotExist(self::PREVIEWS_STORAGE);

            $file_name = Str::random(32) . "." . $file->extension();

            $this->model->preview_media = $file_name;

            Storage::disk('public')->putFileAs(self::PREVIEWS_STORAGE, $file, $file_name);
            $this->log("Upload file: " . self::PREVIEWS_STORAGE . $file_name);
        }

        return $this;
    }

    public function setTagsList($tags_list): CoreBuilder {
        if ($tags_list) {
            $tags_list = explode('/', $tags_list);
            $formatted_tags_list = [];

            foreach ($tags_list as $tag) {
                $formatted_tags_list[] = trim($tag);
            }

            $this->model->tags_list = json_encode($formatted_tags_list);
        } else {
            $this->model->tags_list = null;
        }

        return $this;
    }
}
