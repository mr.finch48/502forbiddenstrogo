<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LiveStreamCollection extends ResourceCollection
{
    use PaginatedResource;

    public static $wrap = 'live_stream';

    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
