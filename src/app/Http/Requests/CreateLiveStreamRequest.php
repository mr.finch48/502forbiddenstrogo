<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateLiveStreamRequest extends CustomFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'live_stream' => 'nullable|url',
            'title' => 'required|string',
            'description' => 'nullable|string',
            'start_date_time' => 'required|before_or_equal:end_date_time|date_format:"Y-m-d\TH:i"',
            'end_date_time' => 'required|after_or_equal:start_date_time|date_format:"Y-m-d\TH:i"',
            'rubric' => 'required|integer|exists:App\Models\RubricModel,id',
        ];
    }
}
